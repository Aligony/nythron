import Model from './model';

class Game extends Model {

    constructor () {
        super ();

        this.multiplayer = false;
        this.name = '';
        this.price = 0;
        this.type = '';
    }

    isMultiplayer () {
        return this.name;
    }

    setMultiplayer (multiplayer) {
        this.multiplayer = multiplayer;
        return this;
    }

    getName () {
        return this.name;
    }

    setName (name) {
        this.name = name;
        return this;
    }

    getType () {
        return this.type;
    }

    getPrice () {
        return this.price;
    }

    setPrice (price) {
        this.price = price;
        return this;
    }

    setType (type) {
        this.type = type;
        return this;
    }
}

module.exports = Game;
