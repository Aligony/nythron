
class Model {
    constructor () {
        this.id;
        this.createdAt = new Date();
        this.updateAt = new Date();
    }

    setData (data) {
        Object.keys(data).forEach(key => {
            this[key] = data[key];
        });
    }

    getId () {
        return this.id;
    }

    setId (id) {
        this.id = id;
        return this;
    }

    getCreatedAt () {
        return this.createdAt;
    }

    setCreatedAt (createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    getUpdatedAt () {
        return this.updateAt;
    }

    setUpdatedAt (updateAt) {
        this.updateAt = updateAt;
        return this;
    }
}

module.exports = Model;
