require('dotenv').config();
const AppMessage = require('./messages/app');
const { games } = require('./collections');

const { Client } = require('discord.js');
const client = new Client();
const PREFIX = 'nythron ';

client.on('ready', () => console.log(`${client.user.username} has logged in.`));

client.on('message', message => {
    if (message.author.bot) return;

    if (message.content.startsWith(PREFIX)) {
        const [CMD_NAME, ...args] = message.content
            .trim()
            .substring(PREFIX.length)
            .split(/\s+/)
        ;

        switch (CMD_NAME) {
            case 'h':
            case 'help':
                message.channel.send(AppMessage.help.join('\n'));
                break;
            case 'v':
            case 'version':
                message.channel.send(`v${process.env.NYTHRON_CURRENT_VERSION}`);
                break;
            case 'games':
                message.channel.send(games.map(g => g.name));
                break;
        }
    }
});

client.login(process.env.DISCORD_BOT_TOKEN);
