let data = {
    games: [
        {
            multiplayer: true,
            name: 'Age of Empire II: Definitive Edition',
            price: 19.99
        },
        {
            name: 'Darkest Dungeon',
            price: 22.99
        },
        {
            name: 'Dead Cells',
            price: 24.99
        },
        {
            multiplayer: true,
            name: 'Disc Jam',
            price: 14.99
        },
        {
            multiplayer: true,
            name: 'Fall Guys',
            price: 19.99
        },
        {
            multiplayer: true,
            name: 'Krunker.io'
        },
        {
            multiplayer: true,
            name: 'League of Legends'
        },
        {
            multiplayer: true,
            name: 'Minecraft',
            price: 29.99
        },
        {
            multiplayer: true,
            name: 'Minecraft Dungeons',
            price: 19.99
        },
        {
            name: 'Moonlighter',
            price: 19.99
        },
        {
            multiplayer: true,
            name: "No Man's Sky",
            price: 54.99
        },
        {
            multiplayer: true,
            name: 'Overcooked 2',
            price: 22.99
        },
        {
            multiplayer: true,
            name: 'Portal 2',
            price: 8.19
        },
        {
            multiplayer: true,
            name: 'Rocket League',
            price: 19.99
        },
        {
            multiplayer: true,
            name: 'Satisfactory',
            price: 29.99
        },
        {
            multiplayer: true,
            name: "Sid Meier's Civilization V",
            price: 29.99
        },
        {
            multiplayer: true,
            name: "Sid Meier's Civilization VI",
            price: 59.99
        },
        {
            name: 'Startup Company',
            price: 10.79
        },
        {
            multiplayer: true,
            name: 'Stick Fight: The Game',
            price: 4.99
        },
        {
            name: 'Subnautica',
            price: 20.99
        },
        {
            name: 'Subnautica: Below Zero',
            price: 16.79
        },
        {
            name: 'Super Meat Boy',
            price: 13.99
        },
        {
            name: 'SUPERHOT',
            price: 22.99
        },
        {
            name: 'Sword Art Online: Hollow Realization Deluxe Edition',
            price: 49.99
        },
        {
            multiplayer: true,
            name: 'Temtem',
            price: 32.99
        },
        {
            multiplayer: true,
            name: 'Trackmania',
            price: 9.99
        },
        {
            name: 'Wizard of Legend',
            price: 15.99
        }
    ]
};

data.games = data.games.sort((a, b) => a.name.localeCompare(b.name));

module.exports = data;
