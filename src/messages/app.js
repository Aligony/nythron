module.exports = {
    help: [
        'Usage: nythron [command] [arguments]',
        'Commands:',
        '  `h` | `help` print Nythron command line options',
        '  `v` | `version` print Nythron version',
        '  `games` print the list of saved games'
    ]
}
