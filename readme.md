# Nythron

> Author : **ligonya**, ?  
> Version : **1.0.0**  

### Install

```bash
npm i
npm i -g nodemon
```

### Run

```bash
npm run start
```
